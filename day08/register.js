$(document).ready(function() {
    $("#registrationForm").submit(function(event) {
        var usernameValid = true;
        var genderValid = true;
        var departmentValid = true;
        var birthdayValid = true;

        var username = $("#username").val();
        if (username === "") {
            $("#usernameMessage").text("Hãy nhập tên");
            usernameValid = false;
        } else {
            $("#usernameMessage").text("");
        }

        var gender = $("input[name='gender[]']:checked").length;
        if (gender === 0) {
            $("#genderMessage").text("Hãy chọn giới tính");
            genderValid = false;
        } else {
            $("#genderMessage").text("");
        }

        var department = $("#department").val();
        if (department === "PK") {
            $("#departmentMessage").text("Hãy chọn phân khoa");
            departmentValid = false;
        } else {
            $("#departmentMessage").text("");
        }

        var birthday = $("input[type='date']").val();
        if (birthday === "") {
            $("#birthdayMessage").text("Hãy chọn ngày sinh");
            birthdayValid = false;
        } else {
            $("#birthdayMessage").text("");
        }

        if (!usernameValid || !genderValid || !departmentValid || !birthdayValid) {
            event.preventDefault();
        }
    });
});
