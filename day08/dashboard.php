<html>

<head>
    <title>Sinh Viên</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="dashboard.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="register.js"></script>
    <script>
        function searchStudents() {
            var department = document.getElementById('department').value;
            var keyword = document.getElementById('searchInput').value;

            if (department || keyword) {
                $.ajax({
                    type: 'POST',
                    url: 'dashboard.php',
                    data: {
                        department: department,
                        keyword: keyword
                    },
                    success: function(data) {
                        data = removeUnwantedForm(data);
                        $('#studentTable').html(data);
                    }
                });
            }
        }

        function resetForm() {
            var form = document.getElementById("search-form");

            form.elements["department"].selectedIndex = 0;

            form.elements["search"].value = '';
        }

        function removeUnwantedForm(data) {
            var $temp = $('<div>').html(data);
            $temp.find('#search-form').remove();
            $temp.find('.search-button').remove();
            return $temp.html();
        }
    </script>
</head>

<body>
    <?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "ltweb";

    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
        die("Kết nối thất bại: " . $conn->connect_error);
    }
    $NumStudentSql = "SELECT COUNT(*) AS count FROM students;";
    $numStudent = $conn->query($NumStudentSql);
    $numStudentRow = $numStudent->fetch_assoc();
    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["delete"])) {
        $conn = new mysqli($servername, $username, $password, $dbname);
        $id = $_POST["id"];
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        $sql = "DELETE FROM students WHERE id = '$id' ";
        if ($conn->query($sql) === TRUE) {
            header("Location: dashboard.php");
            exit();
        } else {
            echo "Error deleting news article: " . $conn->error;
        }
        $conn->close();
    }
    ?>

    <form id="search-form" action="dashboard.php" method="post">
        <div class="search-container">
            <div class='department-title'>
                <p>Khoa</p>
            </div>
            <select name="department" id="department" onchange="searchStudents()">
                <option>--Chọn Khoa--</option>
                <option value="KHMT">Khoa học máy tính</option>
                <option value="KHVL">Khoa học vật liệu</option>
            </select>
        </div>
        <div class="search-container">
            <div class='keyword-title'>
                <p>Từ khóa</p>
            </div>
            <input name="search" type="text" class='keyword-search' id='searchInput' onkeyup="searchStudents()">
        </div>
    </form>
    <button onclick="resetForm()" name="reset-button" class='search-button'>Reset</button>
    <div id="studentTable">
        <div class="container">
            <p class='num-student'> Số sinh viên tìm thấy: <?php echo $numStudentRow['count'] ?></p>
            <button class="add-students"><a href="register.php" class="add-students-link">Thêm</a></button>
        </div>
        <table>
            <tr>
                <td class='id'>No</td>
                <td class='name'>Tên Sinh Viên</td>
                <td class='department'>Khoa</td>
                <td class='action'>Action</td>
            </tr>

            <?php

            $tableContent = '';
            $searchSql = "";
            if ($_SERVER["REQUEST_METHOD"] == "POST") {

                $department = $_POST['department'];
                $keyword = $_POST['keyword'];
                $selectedDepartment = $_POST["department"];
                if ($selectedDepartment == "--Chọn Khoa--") {
                    $selectedDepartment="";
                } elseif ($selectedDepartment == "KHMT") {
                    $selectedDepartment = "Khoa học máy tính";
                } elseif ($selectedDepartment == "KHVL") {
                    $selectedDepartment = "Khoa học vật lý";
                }
                if ($selectedDepartment){
                 $searchSql = " SELECT * FROM `students` WHERE `department` LIKE '$selectedDepartment' AND (`id` LIKE '%$keyword%' OR `name` LIKE '%$keyword%')";
                }
                else{
                    $searchSql = " SELECT * FROM `students` WHERE `department` LIKE '%$keyword%' OR `id` LIKE '%$keyword%' OR `name` LIKE '%$keyword%'";
                };
            }
            else {
                $searchSql = "SELECT * FROM `students`";
            }
            $studentSql = $conn->query($searchSql);
            while ($row = $studentSql->fetch_assoc()) {
                $id = $row['id'];
                echo "<tr>";
                echo "<td class='id'>" . $row['id'] . "</td>";
                echo "<td class='name'>" . $row['name'] . "</td>";
                echo "<td class='department'>" . $row['department'] . "</td>";
                echo "<td class='action'>     
                <form action='dashboard.php' method='POST'>       
                <input type='hidden' name='id' value='" . $row["id"] . "'>
                <button class='delete' type='submit' name='delete' onclick='return confirm(\"Xóa?\")'> Xóa </button>
                </form>

                <form action='dashboard.php' method='post'>
                <input type='hidden' name='id' value='" . $row["id"] . "'>
                <button class='edit' type='submit' name='edit'>Sửa</button>
                </form>

            </td>";
            }

            ?>
        </table>
    </div>

</body>

</html>