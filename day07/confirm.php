<!DOCTYPE html>
<html lang="en">

<head>
    <title>Form Đăng ký</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  
</head>

<body>
<form id="registrationForm" class="bd-blue" method="POST" enctype="multipart/form-data"action = "insertDatabase.php">

            <div class="form-row">
                <div class="required-label" for="name">Họ và tên</div>
                <p><?php echo $_POST["name"]; 
                    ?></p>
                <input type = "hidden" name = "name" value = "<?php echo $_POST['name'];?>">
            </div>

            <div class="form-row">
                <div class="required-label" for="gender">Giới tính</div>
                <p><?php echo $_POST["gender"]; ?></p>
                <input type = "hidden" name = "gender" value = "<?php echo $_POST['gender'];?>">
            </div>

            <div class="form-row">
                <div class="required-label" for="department">Phân khoa</div>
                <p><?php echo $_POST["department"]; ?></p>
                <input type = "hidden" name = "department" value = "<?php echo $_POST['department'];?>">
            </div>

            <div class="form-row">
                <div class="required-label w-170" for="birthdate">Ngày sinh</div>
                <p><?php echo $_POST["birthdate"]; ?></p>
                <input type = "hidden" name = "birthdate" value = "<?php echo $_POST['birthdate'];?>">
            </div>      
            <div class="d-flex">
                <div class="address-label" for="address">Địa chỉ</div>
                <p><?php echo $_POST["address"]; ?></p>
                <input type = "hidden" name = "address" value = "<?php echo $_POST['address'];?>">
            </div>
            <div class="form-row">
                <div class="required-label w-170" for="image">Hình ảnh</div> 
                <?php
                if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_FILES["image"])) {
                    $uploadDir = "image/"; 
                    $uploadFile = $uploadDir . basename($_FILES["image"]["name"]);
                
                    $imageFileType = strtolower(pathinfo($uploadFile, PATHINFO_EXTENSION));
                    $allowedExtensions = array("jpg", "jpeg", "png", "gif");
                
                    if (in_array($imageFileType, $allowedExtensions)) {
                        if (move_uploaded_file($_FILES["image"]["tmp_name"], $uploadFile)) {
                            echo '<img src="' . $uploadFile . '" alt="Uploaded Image" width = "100px" height = "auto">';
                        } else {
                            echo "Có lỗi xảy ra khi tải lên tệp.";
                        }
                    } else {
                        echo "Vui lòng tải lên tệp hình ảnh có định dạng JPG, JPEG, PNG hoặc GIF.";
                    }
                }
                ?>
                <input type = "hidden" name = "image" value = "<?php echo basename($_FILES["image"]["name"]);?>">
            </div> 

            <div class="button-container" id="registerButton">
                <button type="submit">Xác nhận</button>
            </div>
        </form>

</body>

</html>
