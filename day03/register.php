<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Đăng ký</title>
    <link rel="stylesheet" href="./style.css">
</head>

<body>
    <form action="">
        <table>
            <tr>
                <td class="table-field"><label for="name">Họ và
                        tên</label></td>
                <td class="table-input">
                    <input type="text" id="name" name="name" required>
                </td>
            </tr>
            <tr>
                <td class="table-field"><label for="gender">Giới tính</label>
                </td>
                <td>
                    <div class="genders-radio">
                        <?php
                    $values = array(0 => 'Nam', 1 => 'Nữ');
                    $genders =  array(0 => 'men', 1 => 'women');
                    for ($i = 0; $i < count($genders); $i++) {
                        $key = $genders[$i];
                        $value = $values[$i];
                        echo "<div class='genders'>
                            <input id='$key' type='radio' name='genders' value='$value'>
                            <label for='$key'>$value</label>
                        </div>";
                    }                   

                    ?>
                    </div>

                </td>

            </tr>
            <tr>
                <td class="table-field"><label for="department">Phân
                        khoa</label></td>
                <td>

                    <select id="department" name="department" class="table-select">
                        <option value="">--Chọn phân khoa--</option>
                        <?php
                        $departments = array('MAT' => 'KHMT', 'KDL' => 'KHDL');
                        foreach ($departments as $key => $value) {
                            echo "<option value=\"$key\">$value</option>";
                        }
                        ?>
                    </select>
                </td>
            </tr>
        </table>
        <div class="btn">
            <button type="submit" class="btn-submit">Đăng ký</button>
        </div>
    </form>
</body>

</html> 