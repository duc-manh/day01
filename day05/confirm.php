<!DOCTYPE html>
<html lang="en">

<head>
    <title>Form Đăng ký</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  
</head>

<body>
        <form id="registrationForm" class="bd-blue">
        <div id="errorMessages" class="error">
                <script>
                    $(document).ready(function () {
                        $("#registerButton").click(function () {
                            var name = $("#name").val();
                            var gender = $("input[name='gender']:checked").val();
                            var department = $("#department").val();
                            var birthdate = $("#birthdate").val();

                            var errorMessages = [];

                            if (name === "") {
                                errorMessages.push("Hãy nhập tên.");
                            }

                            if (!gender) {
                                errorMessages.push("Hãy chọn giới tính.");
                            }

                            if (department === "") {
                                errorMessages.push("Hãy chọn phân khoa.");
                            }

                            var datePattern = /^\d{2}\/\d{2}\/\d{4}$/;
                            if (birthdate === "" || !datePattern.test(birthdate)) {
                                errorMessages.push("Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy.");
                            }

                            if (errorMessages.length > 0) {
                                var errorMessageHtml = "";
                                for (var i = 0; i < errorMessages.length; i++) {
                                    errorMessageHtml += errorMessages[i] + "<br>";
                                }
                                $("#errorMessages").html(errorMessageHtml);
                            } else {
                                $("#errorMessages").html("");
                            }
                        });
                    });
                </script>
            </div>

            <div class="form-row">
                <div class="required-label" for="name">Họ và tên</div>
                <p><?php echo $_POST["name"]; ?></p>
            </div>

            <div class="form-row">
                <div class="required-label" for="gender">Giới tính</div>
                <p><?php echo $_POST["gender"]; ?></p>
            </div>

            <div class="form-row">
                <div class="required-label" for="department">Phân khoa</div>
                <p><?php echo $_POST["department"]; ?></p>
            </div>

            <div class="form-row">
                <div class="required-label w-170" for="birthdate">Ngày sinh</div>
                <p><?php echo $_POST["birthdate"]; ?></p>
            </div>      
            <div class="d-flex">
                <div class="address-label" for="address">Địa chỉ</div>
                <p><?php echo $_POST["address"]; ?></p>
            </div>
            <div class="form-row">
                <div class="required-label w-170" for="image">Hình ảnh</div> 
                <?php
                if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_FILES["image"])) {
                    $uploadDir = "image/"; 
                    $uploadFile = $uploadDir . basename($_FILES["image"]["name"]);
                
                    $imageFileType = strtolower(pathinfo($uploadFile, PATHINFO_EXTENSION));
                    $allowedExtensions = array("jpg", "jpeg", "png", "gif");
                
                    if (in_array($imageFileType, $allowedExtensions)) {
                        if (move_uploaded_file($_FILES["image"]["tmp_name"], $uploadFile)) {
                            echo '<img src="' . $uploadFile . '" alt="Uploaded Image" width = "100px" height = "auto">';
                        } else {
                            echo "Có lỗi xảy ra khi tải lên tệp.";
                        }
                    } else {
                        echo "Vui lòng tải lên tệp hình ảnh có định dạng JPG, JPEG, PNG hoặc GIF.";
                    }
                }
                ?>

            </div> 
            <div class="button-container" id="registerButton">
                <button type="submit">Xác nhận</button>
            </div>
        </form>

</body>

</html>
