<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="input_student.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> 
    <script src="register.js"></script>
    <form method="POST" id="registrationForm" action="regist_student.php" enctype="multipart/form-data" name="submitForm" title="Form đăng ký sinh viên">
</head>
<body>
    <div class="container">
        <p class='error' id="usernameMessage" style="color: red;"></p>
        <p class='error' id="genderMessage" style="color: red;"></p>
        <p class='error' id="departmentMessage" style="color: red;"></p>
        <p class='error' id="birthdayMessage" style="color: red;"></p>
        <form method="POST" id="registrationForm" action="regist_student.php" enctype="multipart/form-data" name="submitForm">

            <div class="input-container">
                <div class="user-name" style="display: inline-block; vertical-align: middle;">
                <label for="username"><span class="text">Họ và tên</span> <span class="required">*</span></label>
                </div>
                <div class="input-text" style="display: inline-block; vertical-align: middle;">
                    <input type="text" id="username" name="username" class="blue-rectangle" style="background-color: white; border: none; width: 98%; height: 90%;" required>
                </div>
            </div>    

            <div class="input-container">
                <div class="user-name" style="display: inline-block; vertical-align: middle;">
                <label><span class="text">Giới tính</span> <span class="required">*</span></label>
                </div>
                <div class="genderInput" style="display: inline-block; vertical-align: middle;">
                    <?php
                    $genders = array(0 => 'Nam', 1 => 'Nữ'); 
                    for ($key = 0; $key < count($genders); $key++) {
                        echo '<label class="custom-checkbox">';
                        echo '<input class="" type="radio" id="gender' . $key . '" name="gender[]" value="' . $key . '" required>';
                        echo '<span class="checkmark"></span>';
                        echo $genders[$key];
                        echo '</label>';
                    }
                    ?>
                </div>
            </div>

            <div class="input-container">
                <div class="user-name" style="display: inline-block; vertical-align: middle;">
                <label><span class="text">Ngày sinh</span> <span class="required">*</span></label>
                </div>  
                <div style="display: inline-block; vertical-align: middle;">
                    <select name="day" id="day" class="input" required>
                        <option value="">Ngày</option>
                        <?php
                        for ($day = 1; $day <= 31; $day++) {
                            echo "<option value='{$day}'>{$day}</option>";
                        }
                        ?>
                    </select>

                    <select name="month" id="month" class="input" required>
                        <option value="">Tháng</option>
                        <?php
                        for ($month = 1; $month <= 12; $month++) {
                            echo "<option value='{$month}'>{$month}</option>";
                        }
                        ?>
                    </select>

                    <select name="year" id="year" class="input" required>
                        <option value="">Năm</option>
                        <?php
                        $currentYear = date('Y');
                        $startYear = $currentYear - 40;
                        $endYear = $currentYear - 15;
                        for ($year = $endYear; $year >= $startYear; $year--) {
                            echo "<option value='{$year}'>{$year}</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="input-container">
                <div class="user-name" style="display: inline-block; vertical-align: middle;">
                <label for="city"><span class="text">Thành phố</span> <span class="required">*</span></label>
                </div>
                <div style="display: inline-block; vertical-align: middle;">
                    <select name="city" id="city" class="input" required>
                        <option value="">Chọn thành phố</option>
                        <option value="hanoi">Hà Nội</option>
                        <option value="hochiminh">TP.Hồ Chí Minh</option>
                    </select>
                </div>
            </div>

            <div class="input-container">
                <div class="user-name" style="display: inline-block; vertical-align: middle;">
                <label for="district"><span class="text">Quận</span> <span class="required">*</span></label>
                </div>
                <div style="display: inline-block; vertical-align: middle;">
                    <select name="district" id="district" class="input" required>
                        <option value="{blank}">Chọn quận</option>
                    </select>
                </div>
            </div>

            <div class="input-container">
                <div class="user-name" style="display: inline-block; vertical-align: middle;">
                <label for="otherInfo"><span class="text">Thông tin khác</span> <span class="required">*</span></label>
                </div>
                <div style="display: inline-block; vertical-align: middle;">
                    <textarea name="otherInfo" class="input-textarea" rows="4" cols="50" required></textarea>
                </div>
            </div>
            <div class="submit-container">
                <div class="submit-button" style="display: inline-block; vertical-align: middle;">
                    <button type="button" id="submitBtn" style="background-color: rgb(115,173,73);color: white; border: none;">Đăng ký</button>
                </div>
            </div>
        </form>
    </div>
    <script>
document.addEventListener("DOMContentLoaded", function() {
    var submitButton = document.getElementById("submitBtn");
    
    submitButton.addEventListener("click", function() {
        var usernameInput = document.getElementById("username");
        var genderInputs = document.querySelectorAll("input[name='gender[]']");
        var daySelect = document.getElementById("day");
        var monthSelect = document.getElementById("month");
        var yearSelect = document.getElementById("year");
        var citySelect = document.getElementById("city");
        var districtSelect = document.getElementById("district");
        var otherInfoTextarea = document.getElementById("otherInfo");
        if (
            !usernameInput.value ||
            !genderInputs[0].checked && !genderInputs[1].checked ||
            daySelect.value === "" || monthSelect.value === "" || yearSelect.value === "" ||
            citySelect.value === "" || districtSelect.value === "" ||
            !otherInfoTextarea.value
        ) {
            alert("Vui lòng điền đầy đủ thông tin.");
        } else {
            window.location.href = "regist-student.php";
        }
    });
});
document.addEventListener("DOMContentLoaded", function() {
    var citySelect = document.getElementById("city");
    var districtSelect = document.getElementById("district");
    
    var districts = {
        hanoi: ["Quận Ba Đình", "Quận Hoàn Kiếm", "Quận Hai Bà Trưng", "Quận Đống Đa", "Quận Cầu Giấy"],
        hochiminh: ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"]
    };

    citySelect.addEventListener("change", function() {
        var selectedCity = citySelect.value;
        districtSelect.innerHTML = "";
        
        for (var i = 0; i < districts[selectedCity].length; i++) {
            var option = document.createElement("option");
            option.value = districts[selectedCity][i];
            option.text = districts[selectedCity][i];
            districtSelect.appendChild(option);
        }
    });
});
    </script>