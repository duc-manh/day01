<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Xác nhận đăng ký</title>
</head>
<body>
    <h1>Thông tin đăng ký</h1>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $username = $_POST["username"];
        $gender = $_POST["gender"];
        $day = $_POST["day"];
        $month = $_POST["month"];
        $year = $_POST["year"];
        $city = $_POST["city"];
        $district = $_POST["district"];
        $otherInfo = $_POST["otherInfo"];
        
        echo "<p><strong>Họ và tên:</strong> $username</p>";
        echo "<p><strong>Giới tính:</strong> $gender</p>";
        echo "<p><strong>Ngày sinh:</strong> $day/$month/$year</p>";
        echo "<p><strong>Thành phố:</strong> $city</p>";
        echo "<p><strong>Quận:</strong> $district</p>";
        echo "<p><strong>Thông tin khác:</strong> $otherInfo</p>";
    } else {
        echo "<p>Không có thông tin đăng ký nào được gửi.</p>";
    }
    ?>
</body>
</html>
